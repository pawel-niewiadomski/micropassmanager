package eu.pawelniewiadomski.java.spring.micropassmanager.standalone;

import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.startup.HostConfig;
import java.util.Optional;


public class TomcatMain {
    private static final Optional<String> port = Optional.ofNullable(System.getenv("PORT"));

    public static void main(String[] args) throws Exception {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(Integer.valueOf(port.orElse("8080")));
      tomcat.setBaseDir("run");
      tomcat.getHost().setAutoDeploy(true);
      //tomcat.getHost().setAppBase("run");
      // This magic line makes Tomcat look for WAR files in run/webapps and automatically deploy them
      tomcat.getHost().addLifecycleListener(new HostConfig());
      //if (args.length > 0 && args[0].equalsIgnoreCase("-startedWithAnt")) {
      //  tomcat.addWebapp("",   "run/webapps/" +  args[1].split("=")[1]);
      //}
        tomcat.start();
        tomcat.getServer().await();

    }
}
